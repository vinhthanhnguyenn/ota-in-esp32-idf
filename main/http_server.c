#include "esp_http_server.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "sys/param.h"


#include "http_server.h"
#include "tasks_config.h"
#include "wifi_app.h"

int switch_count=0;
#define flash_status_true       1
#define flash_status_false      0
#define flash_status_pending    -1
static const char TAG[] = "http_server";

int flash_successful=flash_status_pending;

/**
 * ESP32 timer configuration passed to esp_timer_create.
 */

const esp_timer_create_args_t fw_update_reset_args = {
		.callback = &http_server_fw_update_reset_callback,
		.arg = NULL,
		.dispatch_method = ESP_TIMER_TASK,
		.name = "fw_update_reset"
};
esp_timer_handle_t fw_update_reset;

void http_server_fw_update_reset_timer(void)
{
	
		ESP_LOGI(TAG, "http_server_fw_update_reset_timer: FW updated successful starting FW update reset timer");

		// Give the web page a chance to receive an acknowledge back and initialize the timer
		ESP_ERROR_CHECK(esp_timer_create(&fw_update_reset_args, &fw_update_reset));
		ESP_ERROR_CHECK(esp_timer_start_once(fw_update_reset, 8000000));

}

void http_server_fw_update_reset_callback(void *arg)
{
	ESP_LOGI(TAG, "http_server_fw_update_reset_callback: Timer timed-out, restarting the device");
	esp_restart();
}




// HTTP server task handle
static httpd_handle_t http_server_handle = NULL;

// Queue handle used to manipulate the main queue of events
static QueueHandle_t http_server_queue;

// Embedded files

extern const uint8_t web_server_html_start[] asm("_binary_web_server_html_start");
extern const uint8_t web_server_html_end[]   asm("_binary_web_server_html_end");
extern const uint8_t web_server_css_start[]  asm("_binary_web_css_css_start");   
extern const uint8_t web_server_css_end[]    asm("_binary_web_css_css_end");     
extern const uint8_t web_server_js_start[]   asm("_binary_web_js_js_start");  
extern const uint8_t web_server_js_end[]     asm("_binary_web_js_js_end");  




esp_err_t html_handler(httpd_req_t *req)
{
    ESP_LOGI(TAG, "web_server.html requested");

    httpd_resp_set_type(req, "text/html");
    httpd_resp_send(req,(const char *)web_server_html_start,web_server_html_end-web_server_html_start);

    return ESP_OK;
}   

esp_err_t css_handler(httpd_req_t *req)
{
    ESP_LOGI(TAG, "web_server.css requested");

    httpd_resp_set_type(req, "text/css");
    httpd_resp_send(req,(const char *)web_server_css_start,web_server_css_end-web_server_css_start);

    return ESP_OK;
}   

esp_err_t js_handler(httpd_req_t *req)
{
    ESP_LOGI(TAG, "web_server.js requested");

    httpd_resp_set_type(req, "application/javascript");
    httpd_resp_send(req,(const char *)web_server_js_start,web_server_js_end-web_server_js_start);

    return ESP_OK;
}   

esp_err_t OTA_handler(httpd_req_t *req)
{
    esp_ota_handle_t ota_handle;

    ESP_LOGI(TAG, "OTA_handler requested");

    char ota_buff[1024];
    int content_received=0;
    int content_length=req->content_len;
    int received_length;
    bool is_req_body_started=false;
 

    //get location of ota partition

    const esp_partition_t *update_partition = esp_ota_get_next_update_partition(NULL);

     do
     {
         /* code */
         if((received_length=httpd_req_recv(req,ota_buff,MIN(content_length,1024)))<0)
        {
                if (received_length == HTTPD_SOCK_ERR_TIMEOUT)
			{
				ESP_LOGI(TAG, "http_server_OTA_update_handler: Socket Timeout");
				continue; ///> Retry receiving if timeout occurred
			}
			ESP_LOGI(TAG, "http_server_OTA_update_handler: OTA other Error %d", received_length);
			return ESP_FAIL;


        }

        printf("OTA_handler: OTA RX: %d of %d\r", content_received, content_length);

        if(!is_req_body_started)
        {
            
            
            char *start_content=strstr(ota_buff,"\r\n\r\n")+4;
            int len_con = received_length- (start_content-ota_buff);
            is_req_body_started=true;
    
            

            printf("OTA_handler: OTA file size: %d\r\n", content_length);

            esp_err_t err = esp_ota_begin(update_partition,OTA_SIZE_UNKNOWN,&ota_handle);
            if (err != ESP_OK)
			{
				printf("OTA_handler: Error with OTA begin, cancelling OTA\r\n");
				return ESP_FAIL;
			}

            else
			{
				printf("OTA_handler: Writing to partition subtype %d at offset 0x%x\r\n", update_partition->subtype, update_partition->address);
			}

            esp_ota_write(ota_handle,start_content,len_con);
            content_received+=len_con;
        }

        else
        {
             esp_ota_write(ota_handle,ota_buff,received_length);
             content_received+=received_length;   


        }

     } while (received_length>0 && content_received<content_length);

     // OTA end
     if(esp_ota_end(ota_handle)==ESP_OK)
     {
         if (esp_ota_set_boot_partition(update_partition) == ESP_OK)
         {

            flash_successful=flash_status_true;
            ESP_LOGI("OTA_handler","OTA UPDATE SUCCESFULLY !");

         }

         else
		{
            flash_successful=flash_status_false;
		}

     }

     if(flash_successful==flash_status_true)
     {
        http_server_fw_update_reset_timer();
     }

     else if(flash_successful==flash_status_false)
     {  
        ESP_LOGI("OTA_handler","OTA_UPDATE_FAILED !");
     }

     return ESP_OK;
     
}

esp_err_t OTA_status_handler(httpd_req_t* req)
{
    char otaJSON[100];

	ESP_LOGI(TAG, "OTAstatus requested");

	sprintf(otaJSON, "{\"is_flash_successful\":%d}",flash_successful);

	httpd_resp_set_type(req, "application/json");
	httpd_resp_send(req, otaJSON, strlen(otaJSON));

	return ESP_OK;



}



httpd_handle_t http_server_config(void)
{
    httpd_config_t config=HTTPD_DEFAULT_CONFIG();

    config.core_id=HTTP_APP_TASK_CORE_ID;
    config.task_priority=HTTP_APP_TASK_PRIORITY;
    	// Bump up the stack size (default is 4096)
    config.stack_size=HTTP_APP_TASK_STACK_SIZE;

    config.max_uri_handlers=20;

    config.recv_wait_timeout = 10;
	config.send_wait_timeout = 10;

    ESP_LOGI(TAG,
			"http_server_configure: Starting server on port: '%d' with task priority: '%d'",
			config.server_port,
			config.task_priority);

        if(httpd_start(&http_server_handle,&config)==ESP_OK)
        {
                    ESP_LOGI(TAG, "http_server_configure: Registering URI handlers");

                    // register web_server.html handler
            httpd_uri_t web_server_html = {
                    .uri = "/",
                    .method = HTTP_GET,
                    .handler = html_handler,
                    .user_ctx = NULL
            };
            httpd_register_uri_handler(http_server_handle,&web_server_html);

                    // register web_server.css handler
            httpd_uri_t web_server_css = {
                    .uri = "/web_css.css",
                    .method = HTTP_GET,
                    .handler = css_handler,
                    .user_ctx = NULL
            };
            httpd_register_uri_handler(http_server_handle,&web_server_css);

                     // register web_server.js handler
            httpd_uri_t web_server_js = {
                    .uri = "/web_js.js",
                    .method = HTTP_GET,
                    .handler = js_handler,
                    .user_ctx = NULL
            };
            httpd_register_uri_handler(http_server_handle,&web_server_js);

            httpd_uri_t OTA_uri=
            {
                    .uri = "/OTA_Handle",
                    .method = HTTP_POST,
                    .handler = OTA_handler,
                    .user_ctx = NULL


            };

             httpd_register_uri_handler(http_server_handle,&OTA_uri);


             httpd_uri_t OTA_status_uri=
            {
                    .uri = "/OTA_Status",
                    .method = HTTP_POST,
                    .handler = OTA_status_handler,
                    .user_ctx = NULL


            };

             httpd_register_uri_handler(http_server_handle,&OTA_status_uri);

            return http_server_handle;

        }

        return NULL;
}

void http_server_start(void)
{
	if (http_server_handle == NULL)
	{
		http_server_handle = http_server_config();
	}
}

void http_server_stop(void)
{
    if (http_server_handle)
	{
		httpd_stop(http_server_handle);
		ESP_LOGI(TAG, "http_server_stop: stopping HTTP server");
		http_server_handle = NULL;
	}
}




