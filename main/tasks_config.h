#ifndef MAIN_TASKS_CONFIG_H_
#define MAIN_TASKS_CONFIG_H_

// WiFi application task
#define WIFI_APP_TASK_STACK_SIZE			4096
#define WIFI_APP_TASK_PRIORITY				5
#define WIFI_APP_TASK_CORE_ID				0

// HTTP application task
#define HTTP_APP_TASK_STACK_SIZE			8192
#define HTTP_APP_TASK_PRIORITY				4
#define HTTP_APP_TASK_CORE_ID				0

#endif
