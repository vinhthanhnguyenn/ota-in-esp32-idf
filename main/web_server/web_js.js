var seconds 	= null;
var otaTimerVar =  null;
var percentComplete=0;



function getFileInfo()
{
    var x=document.getElementById("selected_file");
    var file_recv=x.files[0];

    document.getElementById("file_infor").innerHTML="<b>File name:"+ file_recv.name + "<br>" + "Size:" + file_recv.size +"bytes </b>";
}

function updateFirmware()
{
    var form=new FormData();
    var fileSelected = document.getElementById("selected_file");
    if(fileSelected.files && fileSelected.files.length == 1 )
    {
        var file_var=fileSelected.files[0];
        form.set("file",file_var,file_var.name);
        var xhr=new XMLHttpRequest();
        xhr.upload.addEventListener("progress", updateProgress);
        xhr.open('POST',"/OTA_Handle");
        xhr.responseType="blob";
        xhr.send(form);

    }

    else
    {
        alert("Choose a file first !");
    }



}


function updateProgress(e)
{
    if(e.lengthComputable)
    {
        var String_print;
        percentComplete = e.loaded / e.total;
        String_print="<br>" +"Progress: " + (percentComplete)*100+"%" + "<br>";
        document.getElementById("ota_update_status").innerHTML=String_print ;
        

        if(percentComplete>0.99)
        {
            getUpdateStatus();
        }
    }

    else 
	{
       alert('total size is unknown');
    }


}


function getUpdateStatus()
{
    var xhr = new XMLHttpRequest();
    var requestURL="/OTA_Status";
    xhr.open('POST', requestURL,false);
    xhr.send('is_flash_successful');



    if (xhr.readyState == 4 && xhr.status == 200) 
	{		
        var response = JSON.parse(xhr.responseText);					
		// If flashing was complete it will return a 1, else -1
		// A return of 0 is just for information on the Latest Firmware request
        if (response.is_flash_successful == 1) 
		{
            document.getElementById("ota_update_status").innerHTML = "<br>  Upload finished ! Restart in a few seconds";
        } 
        else if (response.is_flash_successful == 0)
		{
            document.getElementById("ota_update_status").innerHTML = "!!! Upload Error !!!";
        }
    }



}











