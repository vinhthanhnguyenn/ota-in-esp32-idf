#ifndef HTTP_SERVER_H_
#define HTTP_SERVER_H_

void http_server_start(void);
void http_server_stop(void);
void http_server_fw_update_reset_timer(void);
void http_server_fw_update_reset_callback(void *arg);

#endif